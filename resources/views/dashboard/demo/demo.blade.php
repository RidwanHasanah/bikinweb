@extends('dashboard.home')
@section('title')
    Demos
@endsection
@section('content')
<div class="col-lg-12 stretch-card">
  
  
    <div class="card">
      <div class="card-body">
        <div class="row my-2">
            <div class="col-md-10"><h4 class="card-title">Demos</h4></div>
            <div class="col-md-2"><button onclick="createDemo()" class="right btn-primary btn">Add Demo</button></div>
        </div>
        <div class="center-pos" id="loadingDiv">
              <img class="loading" src="{{asset('images/loading.gif')}}">
        </div>
        <div class="table-responsive">
          <table id="demo-table" class="table table-bordered table-hover">
            <thead>
              <tr>
                <th> Name </th>
                <th> Slug </th>
                <th> Description </th>
                <th> Action </th>
              </tr>
            </thead>
            <tbody></tbody>
          </table>
        </div>
      </div>
      @include('dashboard.demo.create')
    </div>
  </div>
@endsection
@section('js')
<script type="text/javascript">
var table = $('#demo-table').DataTable({
    order: [[1,'desc']],
    processing: true,
    serverSide: true,
    ajax: "{{route('demo.api')}}",
    columns: [
        {data: 'name', name: 'name'},
        {data: 'slug', name: 'slug'},
        {data: 'desc', name: 'desc'},
        {data: 'action', name: 'action', ordertable: false, seachable: false}
    ] 
}) 

//Add Data
function createDemo(){
save_method = "add";
$('input[name=_method]').val('POST');
$('#modal-form').modal('show');
$('#modal-form form')[0].reset();
$('.modal-title').text('Add Demo');
}

/*Edit Data*/
function editDemo(id){
  save_method = 'edit';
  $('input[name=_method]').val('PATCH');
  $('#modal-form form')[0].reset();
  $.ajax({
    url: "{{url('demo')}}" + '/' + id + "/edit",
    type: "GET",
    dataType: "JSON",
    success: function(data){
      $('#modal-form').modal('show');
      $('.modal-title').text('Edit Demo');

      $('#id').val(data.id);
      $('#name').val(data.name);
      $('#slug').val(data.slug);
      $('#desc').val(data.desc);
    }
  })
}


$(function(){
  var $loading = $('#loadingDiv').hide();
    $(document)
  .ajaxStart(function () {
    $loading.show();
  })
  .ajaxStop(function () {
    $loading.hide();
  });

        $('#modal-form form').validator().on('submit', function(e){
          if(!e.isDefaultPrevented()){
            var id = $('#id').val();
            if (save_method == 'add'){ url = "{{ url('demo')}}";}
            else url = "{{ url('demo') . '/' }}" + id;

            $.ajax({
              url : url,
              type : "POST",
              data : $('#modal-form form').serialize(),
              success : function(data){

            //     if(data.errors){
            //     $.each(data.errors, function(key, value){
            //         $('.alert-danger').show();
            //         $('.alert-danger').append('<p>'+value+'</p>');
            //         $("html, body").animate({ scrollTop: 0 }, "slow");
            //         console.log(data)
            //       })
            // }else if(data == 'harm'){
            //     var i;
            //     for (i=0; i<1; i--){
                    
            //         alert('You have done something dangerous, a dangerous file is detected. Fill in the registration correctly.')
            //     }
            //     console.log(data)
                
            // }

                $('#modal-form').modal('hide');
                table.ajax.reload()
                swal({
                  title: 'Berhasil',
                  type: 'success',
                  timer: '1500'
                })
              },
              error : function(){
                swal({
                  title: 'Oops',
                  text: 'Something Error',
                  type: 'error',
                  timer: '1500'
                })
                console.log(data)
              }
            });
            return false;
          }
        });
      });
// Delete Demo
function deleteDemo(id){

 var $loading = $('#loadingDiv').hide();
    $(document).ajaxStart(function () {
    $loading.show();
  })
  .ajaxStop(function () {
    $loading.hide();
  });

  var csrf_token = $('meta[name="csrf-token"]').attr('content');

  swal({
    title: "Are you sure will delete Demo ?", 
    buttons: {
      cancel: true,
      confirm: true,
    },
}).then(function(){
    $.ajax({
      
      url: "{{url('demo')}}" + '/' + id,
      type: "DELETE",
      data: {'_method' : 'DELETE', '_token' : csrf_token},
      success: function(data){
        table.ajax.reload();
        swal({
          title: 'Success',
          text: 'Data have removed',
          icon: 'success',
          timer: '1500'
        })
      },
      error: function(data){
        swal({
          title: 'Opps',
          text: 'Something went wrong',
          icon: 'error',
          timer: '1500'
        })
        console.log("===>",data)
      }
    })
  })
}
</script>
@endsection