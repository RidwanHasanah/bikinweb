<div class="modal" id="modal-form" tabindex="1" role="dialog" aria-hidden="true" data-backup="static">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="alert-danger"></div>
                <form method="post" class="form-horizontal" data-toggle="validator" enctype="multipart/form-data">
                    {{csrf_field()}} {{ method_field('POST')}}
                    <div class="modal-header">
                            <h4 class="modal-title"></h4>
                        <button type="button" class="btn btn-default" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                        
                    </div>
    
                    <div class="modal-body">
                        <input type="hidden" name="id" id="id">
                        <div class="form-group row">
                            <label for="name" class="col-md-3 control-label">Name </label>
                            <div class="col-md-9">
                                <input type="text" name="name" id="name" class="form-control" autofocus required>
                                <span class="help-block with-errors"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-3 control-label">Link Demo </label>
                            <div class="col-md-9">
                                <input type="link" name="slug" id="slug" class="form-control" autofocus required>
                                <span class="help-block with-errors"></span>
                            </div>
                        </div>
    
                        <div class="form-group row">
                            <label for="desc" class="col-md-3 control-label">Description</label>
                            <div class="col-md-9">
                                <textarea type="text" name="desc" id="desc" class="form-control"></textarea>
                                <span class="help-block with-errors"></span>
                            </div>
                        </div>
    
                        <div class="form-group row">
                            <label for="img" class="col-md-3 control-label">Image</label>
                            <div class="col-md-9">
                                <input type="file" accept="image/jpeg,image/jpg,image/png" name="img" id="img" class="form-control">
                                <span class="help-block with-errors"></span>
                            </div>
                        </div>
    
                        <div class="form-group row">
                            <label for="cat" class="col-md-3 control-label">Caegory</label>
                            <div class="col-md-9">
                                <select class="form-control" name="cat" id="cat">
                                    <option value="1">Blogger</option>
                                    <option value="2">Profile</option>
                                    <option value="3">Online Shop</option>
                                    <option value="4">Charity</option>
                                    <option value="5">Education</option>
                                    <option value="6">Health</option>
                                    <option value="7">Food</option>
                                </select>
                                <span class="help-block with-errors"></span>
                            </div>
                        </div>
                    </div>
    
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary btn-save" >Submit</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>