<div id="contact" class="container conprice">
    <h1 class="text-center py-4 card-title h1">Contact</h1>
  <div class="row justify-content-center my-5 text-center">
      <div class="col-lg-6 col-md-6 m-0 p-0">
        <div class=" contact col-lg-12 col-md-12 m-0 p-5 lighten-1 w-100 ani-zoom2  text-white wow  rotateInDownRight slower" data-wow-delay="0.3s" >
          <h3><i class="fa fa-envelope" aria-hidden="true"></i>&nbsp; Email</h3>
          <h2>bikinweb.info@gmail.com</h2>
        </div>
        <div class="col-lg-12 col-md-12 m-0 p-5 w-100 text-primary wow  rotateInUpLeft slower" data-wow-delay="0.3s">
            <h3><i class="fa fa-phone-square" aria-hidden="true"></i>&nbsp; Telephone Seluler</h3>
            <h2>085714442664</h2>
        </div>
      </div>
      <div class="col-lg-6 col-md-6 m-0 p-0 ">
          <div class="col-lg-12 col-md-12 m-0 p-5 w-100 text-primary  wow  rotateInUpRight slower" data-wow-delay="0.3s">
              <h3><i class="fa fa-whatsapp" aria-hidden="true"></i>&nbsp; Whats App</h3>
              <h2>085714442664</h2>
          </div>
          <div class="col-lg-12 col-md-12 m-0 p-5 contact lighten-1 w-100 ani-zoom2 text-white wow  rotateInDownLeft slower" data-wow-delay="0.3s">
              <h3><i class="fa fa-envelope-o" aria-hidden="true"></i>&nbsp; SMS</h3>
              <h2>085714442664</h2>
          </div>
      </div>
  </div>
</div>