 <!-- Demo Website Start -->
 <div id="demo" class="container py-5 my-5 ">
    <h1 class="text-center card-title h1">Demo Website BikinWeb</h1>

    <!-- Grid row -->
    <div class="row">

      <!-- Grid column -->
      <div class="col-md-6 mb-4 slideInUp slower wow" data-wow-delay="0.3s"">

        <!--Card-->
        <div class="card default-color-dark ">

          <!--Card image-->
          <div class="view">
            <img src="{{asset('/images/pondokprogrammer.png')}}" class="card-img-top" alt="photo">
            <a href="https://pondokprogrammer.com" target="_blank">
              <div class="mask rgba-white-slight"></div>
            </a>
          </div>

          <!--Card content-->
          <div class="card-body text-center">
            <!--Title-->
            <h4 class="card-title white-text">Pondok Programmer</h4>
            <!--Text-->
            <a href="https://pondokprogrammer.com" target="_blank" class="btn btn-outline-white btn-md waves-effect">Detail</a>
          </div>

        </div>
        <!--/.Card-->

      </div>
      <!-- Grid column -->

      <!-- Grid column -->
      <div class="col-md-6 mb-4 slideInLeft slower wow" data-wow-delay="0.3s">

        <!--Card-->
        <div class="card primary-color-dark">

          <!--Card image-->
          <div class="view">
            <img src="{{asset('/images/pondokit.png')}}" class="card-img-top"
              alt="photo">
            <a href="https://pondokit.com" target="_blank">
              <div class="mask rgba-white-slight"></div>
            </a>
          </div>

          <!--Card content-->
          <div class="card-body text-center">
            <!--Title-->
            <h4 class="card-title white-text">Pondok IT</h4>
            <a href="https://pondokit.com" target="_blank" class="btn btn-outline-white btn-md waves-effect">Detail</a>
          </div>

        </div>
        <!--/.Card-->

      </div>
      <!-- Grid column -->

    </div>
    <!-- Grid row -->

    <div class="row">

      <!-- Grid column -->
      <div class="col-md-6 mb-4 slideInRight slower wow" data-wow-delay="0.3s">

        <!--Card-->
        <div class="card success-color-dark">

          <!--Card image-->
          <div class="view">
            <img src="{{asset('/images/aatourtravel.png')}}" class="card-img-top" alt="photo">
            <a href="https://aatourtravel.id" target="_blank">
              <div class="mask rgba-white-slight"></div>
            </a>
          </div>

          <!--Card content-->
          <div class="card-body text-center">
            <!--Title-->
            <h4 class="card-title white-text">AA Tour & Travel</h4>
            <a href="https://aatourtravel.id" target="_blank" class="btn btn-outline-white btn-md waves-effect">Detail</a>
          </div>

        </div>
        <!--/.Card-->

      </div>
      <!-- Grid column -->

      <!-- Grid column -->
      <div class="col-md-6 mb-4 slideInUp slower wow" data-wow-delay="0.3s">

        <!--Card-->
        <div class="card info-color-dark">

          <!--Card image-->
          <div class="view">
            <img src="{{asset('/images/baznasbone.png')}}" class="card-img-top" alt="photo">
            <a href="https://baznasbone.org" target="_blank">
              <div class="mask rgba-white-slight"></div>
            </a>
          </div>

          <!--Card content-->
          <div class="card-body text-center">
            <!--Title-->
            <h4 class="card-title white-text">Baznas Bone</h4>
            <a href="https://https://baznasbone.org" target="_blank" class="btn btn-outline-white btn-md waves-effect">Detail</a>
          </div>

        </div>
        <!--/.Card-->

      </div>
      <!-- Grid column -->

    </div>
    <!-- Grid row -->
    <div class="text-center">
      <a id="to-demo" class=" btn btn-danger">Semua Demo Website</a>
    </div>
  </div>
  <!-- Demo Website End -->