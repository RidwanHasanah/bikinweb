@extends('front-page.home')
@section('content')
<div id="main">
    @include('front-page.main.jumbotron')
    @include('front-page.main.about')
    @include('front-page.main.price')
    @include('front-page.main.demo')
    @include('front-page.main.faq')
    @include('front-page.main.contact')
</div>
<div id="all-demo" class="hide">
    @include('front-page.demo.index')
</div>
@endsection