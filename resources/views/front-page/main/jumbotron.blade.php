<!-- Jumbotron -->
<div class="jumbotron text-center blue-grey lighten-5 my-5">

    <!-- Title -->
    <h1 class="card-title fasterone-font">BIKINWEB</h1>

    <!-- Subtitle -->
    <p class="indigo-text my-4 font-weight-bold baloo-font">Mau BikinWeb Tapi bingung seperti apa ? Bikin aja DISINI <a href="bikinweb.pondokprogrammer.com">bikinweb.pondokprogrammer.com</a></p>

    <!-- Grid row -->
    <div class="row d-flex justify-content-center">

      <!-- Grid column -->
      <div class="col-xl-9 pb-2 baloo-font">

        <p class="card-text">
          Kamu punya perusahaan tapi ga punya web profile ? Bikinweb aja disini. <br>
          Kamu suka nulis dan ingin tulisanmu tampil di website agar semua orang bisa lihat dan baca tulisanmu? Bikinweb aja disini. <br>
          Kamu UKM dan ingin mengiklankan produk mu menjadi online ? BikinWeb aja disini.

        </p>
        <p></p>

      </div>
      <!-- Grid column -->

    </div>
    <!-- Grid row -->

    <hr class="my-4 pb-2">

    <a href="#price" class="btn btn-indigo blue-gradient btn-rounded ani-zoom2">Harga Paket<i class="fa fa-diamond ml-1"></i></a>
    <a href="#demo" class="btn btn-indigo btn-rounded ani-zoom2">Demo Website <i class="fa fa-eye ml-1"></i></a>

  </div>
  <!-- Jumbotron -->