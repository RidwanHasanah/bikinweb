 <!-- Price Star -->
 <div id="price" class="container-fluid conprice py-5 my-5 blue-grey lighten-5">
    <h1 class="text-center card-title h1">Harga Paket Pembuatan Website</h1>
    <div class="row justify-content-center">
      <div class="col-sm-6 col-xs-12 col-lg-2 col-md-2 m-0 p-0 wow  slideInRight slower" data-wow-delay="0.3s">
        <ul class="price">
          <li class="price-name-title">Harga</li>
          <li class="price-name-subtitle ani-zoom2">Harga Awal</li>
          <li>Domain(.com/.net)</li>
          <li class="price-names ani-zoom2">Revisi</li>
          <li>Hosting</li>
          <li class="price-names ani-zoom2">Free Support</li>
          <li>Akun Email</li>
          <li class="price-names ani-zoom2">Free Subdomain</li>
          <li>Multi Admin</li>
          <li class="price-names ani-zoom2">SSL</li>
          <li>Kapasitas Halaman</li>
          <li class="price-names ani-zoom2">Harga Perpanjang</li>
        </ul>
      </div>
      <div class="col-sm-6 col-xs-12 col-lg-2 col-md-2 m-0 p-0 wow slideInLeft slower " data-wow-delay="0.3s">
        <ul class=" text-white price ani-zoom">
          <li class="basic-title"><b>Basic</b></li>
          <li class="basic-subtitle">Rp <b>1jt/Tahun</b></li>
          <li class="basic"><i class="fa fa-check"></i></li>
          <li class="basic2"><i class="fa fa-close"></i></li>
          <li class="basic txbold">250mb</li>
          <li class="basic2"><i class="fa fa-check"></i></li>
          <li class="basic"><i class="fa fa-close"></i></li>
          <li class="basic2"><i class="fa fa-close"></i></li>
          <li class="basic"><i class="fa fa-check"></i></li>
          <li class="basic2"><i class="fa fa-check"></i></li>
          <li class="basic txbold">Unlimited</li>
          <li class="basic2">500 rb</li>
        </ul>
      </div>
      <div class="col-sm-6 col-xs-12 col-lg-2 col-md-2 m-0 p-0 wow slideInUp slower" data-wow-delay="0.3s">
        <ul class=" text-white price ani-zoom">
          <li class="standard-title"><b>Standard</b></li>
          <li class="standard-subtitle">Rp <b>1,5jt/Tahun</b></li>
          <li class="standard"><i class="fa fa-check"></i></li>
          <li class="standard2"><i class="fa fa-close"></i></li>
          <li class="standard txbold">500mb</li>
          <li class="standard2"><i class="fa fa-check"></i></li>
          <li class="standard txbold">1</li>
          <li class="standard2 txbold">1</li>
          <li class="standard"><i class="fa fa-check"></i></li>
          <li class="standard2"><i class="fa fa-check"></i></li>
          <li class="standard txbold">Unlimited</li>
          <li class="standard2">600 rb</i></li>
        </ul>
      </div>
      <div class="col-sm-6 col-xs-12 col-lg-2 col-md-2 m-0 p-0 wow slideInLeft slower" data-wow-delay="0.3s">
        <ul class=" text-white price ani-zoom">
          <li class="premium-title"><b>Premium</b></li>
          <li class="premium-subtitle">Rp <b>2jt/Tahun</b></li>
          <li class="premium"><i class="fa fa-check"></i></li>
          <li class="premium2"><i class="fa fa-close"></i></li>
          <li class="premium txbold">750mb</li>
          <li class="premium2"><i class="fa fa-check"></i></li>
          <li class="premium txbold">2</li>
          <li class="premium2 txbold">2</li>
          <li class="premium"><i class="fa fa-check"></i></li>
          <li class="premium2"><i class="fa fa-check"></i></li>
          <li class="premium txbold">Unlimited</li>
          <li class="premium2">800 rb</li>
        </ul>
      </div>
      <div class="col-sm-6 col-xs-12 col-lg-2 col-md-2 m-0 p-0 wow slideInUp slower" data-wow-delay="0.3s">
        <ul class=" text-white price ani-zoom">
          <li class="extra-title"><b>Extra</b></li>
          <li class="extra-subtitle">Rp <b>2,5jt/Tahun</b></li>
          <li class="extra"><i class="fa fa-check"></i></li>
          <li class="extra2"><i class="fa fa-check"></i></li>
          <li class="extra txbold">1gb</li>
          <li class="extra2"><i class="fa fa-check"></i></li>
          <li class="extra txbold">3</li>
          <li class="extra2 txbold">5</li>
          <li class="extra"><i class="fa fa-check"></i></li>
          <li class="extra2"><i class="fa fa-check"></i></li>
          <li class="extra txbold">Unlimited</li>
          <li class="extra2 txbold">1jt</li>
        </ul>
      </div>
    </div>
  </div>
  <!-- Price End -->