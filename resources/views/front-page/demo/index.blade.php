<div id="index-demo">
    <div id="demo" class="container my-5">
        <h1 class="text-center py-4 card-title h1">Demo Website BikinWeb</h1>
        <div>
            <ul class="demo-menu mb-3">
                <li style="background-color: #33b5e5;" id="all-menu">All</li>
                <li id="blogger-menu">Blogger</li>
                <li id="olshop-menu">Online Shop</li>
                <li id="profile-menu">Profile</li>
                <li id="charity-menu">Charity</li>
                <li id="education-menu">Education</li>
                <li id="health-menu">Health</li>
                <li id="food-menu">Food</li>
            </ul>
        </div>
        <div id="show-all">
            @include('front-page.demo.all')
        </div>
        <div class="hide" id="show-blogger">
            @include('front-page.demo.blog')
        </div>
        <div class="hide" id="show-olshop">
            @include('front-page.demo.olshop')
        </div>
        <div class="hide" id="show-profile">
            @include('front-page.demo.profile')
        </div>
        <div class="hide" id="show-charity">
            @include('front-page.demo.charity')
        </div>
        <div class="hide" id="show-education">
            @include('front-page.demo.education')
        </div>
        <div class="hide" id="show-health">
            @include('front-page.demo.health')
        </div>
        <div class="hide" id="show-food">
            @include('front-page.demo.food')
        </div>
    </div>
</div>