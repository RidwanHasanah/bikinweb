<div class="row justify-content-center">
    @foreach ($demoCharity as $demo)
    <div class="col-md-4 mb-4 slideInUp slower wow" data-wow-delay="0.3s" ">
        <div class="card default-color-dark ">
          <div class="view ">
            <img src="{{asset('storage/imgs').'/'.$demo->img}}" class="card-img-top " alt="photo ">
            <a href="{{$demo->slug}}" target="_blank ">
              <div class="mask rgba-white-slight "></div>
            </a>
          </div>
          <div class="card-body text-center ">
            <h4 class="card-title white-text ">{{$demo->name}}</h4>
            <p class="text-white">{{$demo->desc}}</p>
            <a href="{{$demo->slug}}" target="_blank " class="btn btn-outline-white btn-md waves-effect ">Demo</a>
          </div>
        </div>
      </div>
    @endforeach
  </div>