<nav class="navbar navbar-expand-lg navbar-dark primary-color menu">
    <a id="brand-logo" class="navbar-brand" href="#">
      <img src="{{asset('asset/front-page/img/pondokprogrammer-logo.png')}}" height="30" alt="mdb logo">
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav"
      aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a id="home-menu" class="nav-link" href="#">Home</a>
        </li>
        <li class="nav-item">
          <a id="contact-menu" class="nav-link" href="#contact">Contact</a>
        </li>
        <li class="nav-item">
          <a id="price-menu" class="nav-link" href="#price">Pricing</a>
        </li>
        {{-- <li class="nav-item">
          <a class="nav-link" href="#faq">FAQ</a>
        </li> --}}
        <li class="nav-item">
          <a id="demo-menu" class="nav-link" href="#demo">Tema</a>
        </li>
        <li class="nav-item">
          <a id="about-menu" class="nav-link" href="#about">About</a>
        </li>
      </ul>
    </div>
  </nav>