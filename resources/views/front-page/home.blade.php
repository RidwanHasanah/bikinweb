<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>BikinWeb</title>
  <meta name="google-site-verification" content="Qi0wnNXGrGsYX96da3pHG259RJd9UUKRVQhV-8867RY" />
  <link href="https://fonts.googleapis.com/css?family=Baloo+Thambi|Faster+One|Oleo+Script" rel="stylesheet">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <!-- Bootstrap core CSS -->
  <link href="{{asset('asset/front-page/css/bootstrap.min.css')}}" rel="stylesheet">
  <!-- Material Design Bootstrap -->
  <link href="{{asset('asset/front-page/css/mdb.min.css')}}" rel="stylesheet">
  <!-- Your custom styles (optional) -->
  <link href="{{asset('asset/front-page/css/style.css')}}" rel="stylesheet">
  <link rel="shortcut icon" type="images/png" href="{{asset('asset/front-page/img/logo.png')}}"/>
</head>

<body class="oleo-font">

  <!-- Start your project here-->
  @include('front-page.menu')
  @yield('content')
  
  <footer class="container-fluid" style="background-color: #4285F4;">
    <div class="row text-white p-2">
      <small class="text-center" >Powered by PondokProgrammer Copyright<i class="fa fa-copyright" aria-hidden="true"></i>2018</small>
    </div>  
  </footer>
  <!-- /Start your project here-->


  <!-- SCRIPTS -->
  <!-- JQuery -->
  <script type="text/javascript" src="{{asset('asset/front-page/js/jquery-3.3.1.min.js')}}"></script>
  <!-- Bootstrap tooltips -->
  <script type="text/javascript" src="{{asset('asset/front-page/js/popper.min.js')}}"></script>
  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="{{asset('asset/front-page/js/bootstrap.min.js')}}"></script>
  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="{{asset('asset/front-page/js/mdb.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('asset/front-page/js/front-page.js')}}"></script>
  <script>
  new WOW().init();
  </script>
</body>

</html>