<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDemoCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('demo_categories', function (Blueprint $table) {
            $table->integer('demo_id')->unsigned();
            $table->foreign('demo_id')
                  ->references('id')
                  ->on('demos')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');

            $table->integer('cat_id')->unsigned();
            $table->foreign('cat_id')
                ->references('id')
                ->on('categories')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('demo_categories');
    }
}
