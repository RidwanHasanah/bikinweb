<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(array(
            [
                'name' => 'Ridwan',
                'email' => 'ridwan@gmail.com',
                'password' => bcrypt('rjnlrdinmsalafy')
            ],
            [
                'name' => 'Daud',
                'email' => 'daudn@gmail.com',
                'password' => bcrypt('rjnlrdinmsalafy')
            ]

        ));
    }
}
