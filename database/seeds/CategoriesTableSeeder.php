<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
     DB::table('categories')->insert(array(
         [
             'name' => 'Blog'
         ],
         [
             'name' => 'Profile'
         ],
         [
             'name' => 'Olshop'
         ],
         [
             'name' => 'Charity'
         ],
         [
             'name' => 'Education'
         ],
         [
             'name' => 'Health'
         ],
         [
             'name' => 'Food'
         ]
     ));   
    }
}
