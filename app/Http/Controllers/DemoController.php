<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Models\Demo;
use App\Models\DemoCategory;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Storage;
use App\User;

class DemoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        
    }
    
    public function index()
    {
        
        
        return view('dashboard.demo.demo');
    }
    
    public function create()
    {
        //
    }
    
    public function store(Request $request)
    {
        $demo = new Demo;

        $target = [
          'name' => 'required|max:255',
          'slug' => 'required|max:255',
          'desc' => 'required',
          'img' => 'required|image|mimes:jpeg,png,jpg,gif'   
        ];

        $msg = [
            'name.required' => 'Name is Required',
            'slug.required' => 'Demo Link is Required',
            'desc' => 'Description is Required',
            'img.required' => 'Image is Require',
            'img.image' => 'File type must Image',
            'img.mimes' => 'File type must JPG, jpeg, png, gif'
        ];

        // $validation = \Validator::make($request->all(),$target,$msg);
        // if($validation->passes()){
            $demo->name = $request->name;
            $demo->desc = $request->desc;
            $demo->slug = $request->slug;

            /*$name = explode(' ',$request->name);
            if(count($name) >= 4){
                $demo->slug = $name[0].'-'.$name[1].'-'.$name[2];
            }else{
                $demo->slug = implode('-',$name);
            }*/

        if($request->hasFile('img')){
            $extention =  $request->img->getClientOriginalName();
            $extention1 = explode('.', $extention);
            $extention2 = end($extention1);
            // dd($extention2);
            if ($extention2 == "png" || $extention2 == "jpg" || $extention2 == "jpeg" || $extention2 == "gif") {
                $img = $request->file('img');
                $imgName = Auth::user()->id.rand(1,9).$img->getClientOriginalName();
                $img->storeAs('imgs',$imgName);
                $demo->img = $imgName;
            }else{
                return response()->json(['harm']);;
            }
        }
        $demo->save();

        $demo_cat = new DemoCategory;
        $demo_cat->demo_id = $demo->id;
        $demo_cat->cat_id = $request->cat;
        $demo_cat->save();

        return view('dashboard.demo.demo');

        // }else{
        //     return response()->json(['errors'=>$validation->errors()->all()]);
        // }
    }
    
    public function show($id)
    {
        //
    }
    
    public function edit($id)
    {
        $demo = Demo::find($id);
        return $demo;
    }
    
    public function update(Request $request, $id)
    {
        $demo = DemoCategory::find($id);
        $demo->cat_id = $request->cat;
        $demo->update();
        return $demo;
    }
    
    public function destroy($id)
    {
        $demo = Demo::find($id);
        if (strlen($demo->img) != 0) {
            Storage::delete('imgs/'.$demo->img);
        }
        $demo->delete();
    }

    public function demoApi(){
        $demos =  Demo::all();

        return Datatables::of($demos)->addColumn('action',function($demos){
            return "<a width:6em;' href='' class='btn btn-info btn-outline btn-xs' target='_blank'><i class='fa fa-eye'> Detail</i></a> &nbsp;".
                "<a width:6em;' onclick='editAdmin()' class='btn btn-success btn-outline btn-xs'><i class='fa fa-edit'></i> Edit</a> &nbsp;".
                "<a width:6em;' onclick='deleteDemo(".$demos->id.")' class='btn btn-danger btn-outline btn-xs'><i class='fa fa-trash'></i> Delete</a> &nbsp;";

        })->make(true);
    }
}
