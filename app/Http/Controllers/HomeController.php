<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Demo;
use App\Models\Category;
use App\Models\DemoCategory;
use Illuminate\Support\Facades\DB;
use App\User;
/*
  SELECT C.*, D.*
FROM categories AS C
LEFT JOIN demo_categories as DC ON C.id = DC.cat_id
LEFT JOIN demos as D ON D.id = DC.demo_id
WHERE C.name = "blog"
 */
class HomeController extends Controller
{

    public function index()
    {
        $demo = DB::table('demos')->select('*')->get();
        $demoBlog = DB::table('categories')->select('*')
                    ->leftJoin('demo_categories', 'categories.id', '=','demo_categories.cat_id')
                    ->leftJoin('demos', 'demos.id', '=','demo_categories.demo_id')->where('categories.name','Blog')->get();
        $demoProfile = DB::table('categories')->select('*')
                    ->leftJoin('demo_categories', 'categories.id', '=','demo_categories.cat_id')
                    ->leftJoin('demos', 'demos.id', '=','demo_categories.demo_id')->where('categories.name','Profile')->get();
        $demoOlshop = DB::table('categories')->select('*')
                    ->leftJoin('demo_categories', 'categories.id', '=','demo_categories.cat_id')
                    ->leftJoin('demos', 'demos.id', '=','demo_categories.demo_id')->where('categories.name','Olshop')->get();
        $demoCharity = DB::table('categories')->select('*')
                    ->leftJoin('demo_categories', 'categories.id', '=','demo_categories.cat_id')
                    ->leftJoin('demos', 'demos.id', '=','demo_categories.demo_id')->where('categories.name','Charity')->get();
        $demoEducation = DB::table('categories')->select('*')
                    ->leftJoin('demo_categories', 'categories.id', '=','demo_categories.cat_id')
                    ->leftJoin('demos', 'demos.id', '=','demo_categories.demo_id')->where('categories.name','Education')->get();
        $demoHealth = DB::table('categories')->select('*')
                    ->leftJoin('demo_categories', 'categories.id', '=','demo_categories.cat_id')
                    ->leftJoin('demos', 'demos.id', '=','demo_categories.demo_id')->where('categories.name','Health')->get();
        $demoFood = DB::table('categories')->select('*')
                    ->leftJoin('demo_categories', 'categories.id', '=','demo_categories.cat_id')
                    ->leftJoin('demos', 'demos.id', '=','demo_categories.demo_id')->where('categories.name','Food')->get();

        return  view('front-page.main.index',compact(
            'demo','demoBlog','demoProfile','demoOlshop','demoCharity','demoEducation','demoHealth','demoFood'
        ));
    }

    public function all(){
        
        
        return response()->json($demo);
    }

}
