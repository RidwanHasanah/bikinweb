<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Demo extends Model
{
    protected $guarder = ['id'];
    protected $table = 'demos';
    public $timestamps = false;

    public function categories()
    {
        return $this->belongsToMany('App\Models\Demo', 'demo_categories','demo_id', 'cat_id');
    }

    public function cat()
    {
        return $this->belongsToMany('App\Models\Demo');
    }



    public function catDemo(){
        $this->categories();
    }
}
