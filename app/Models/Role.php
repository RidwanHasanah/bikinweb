<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $guarder = ['id'];
    protected $table = 'roles';

    public function users(){
        return $this->belongsToMany('App\Models\User','role_users','role_id','user_id');
    }
}
