<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $guarded = ['id'];
    protected $table = 'categories';

    public function demos(){
        return $this->belongsToMany('App\Models\Demo', 'demo_categories','demo_id', 'cat_id');
    }
}
