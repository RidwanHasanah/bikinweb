<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DemoCategory extends Model
{
    protected $fillable = ['demo_id', 'cat_id'];
    protected $table = 'demo_categories';

    public $timestamps = false;
}
