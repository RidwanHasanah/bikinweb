<?php

Route::get('/', 'HomeController@index')->name('home');

Auth::routes();

Route::group(['middleware'=>['auth']],function(){
    Route::get('home',function(){
        return view('dashboard.dashboard.dashboard');
    });

    Route::resource('demo', 'DemoController');
    Route::get('demo.api','DemoController@demoApi')->name('demo.api');
});

// Route::get('/home', 'HomeController@index')->name('home');
